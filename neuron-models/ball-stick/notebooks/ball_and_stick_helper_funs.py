#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 12 23:27:41 2020
@author: sreedhar s kumar, tobias gaenswein, alessio buccino
"""
import LFPy
import numpy as np
from pathlib import Path
import scipy.signal


# Helper function to instantiate a cell and changing morphological parameters of the AIS (and other structures)
def instantiate_cell_with_custom_params(morphology_header='../morphologies/ball-and-stick-axon-ais-params-header.hoc',
                                        morphology_body='../morphologies/ball-and-stick-axon-ais-params-body.hoc',
                                        output_morphology_path='../morphologies/morphology-with-params.hoc',
                                        d_dend=2, d_soma=15, d_axon=1, d_ais=1,
                                        l_dend=1000, l_axonp=15, l_ais=30, l_axond=1000,
                                        **cell_kwargs):
    morphology_header = Path(morphology_header)
    morphology_body = Path(morphology_body)
    with morphology_header.open('r') as f:
        morphology_header_txt = f.read()
    with morphology_body.open('r') as f:
        morphology_body_txt = f.read()

    # make substitutions in txt files
    morphology_txt = morphology_header_txt.format(d_dend=d_dend, d_soma=d_soma, d_axon=d_axon, d_ais=d_ais,
                                                  l_dend=l_dend, l_axonp=l_axonp, l_ais=l_ais, l_axond=l_axond)
    
    morphology_txt += morphology_body_txt
    
    morphology_param = Path(output_morphology_path)
    with morphology_param.open('w') as f:
        f.write(morphology_txt)
        
    cell = LFPy.Cell(morphology=str(morphology_param), **cell_kwargs)
    
    return cell

# Helper function to define the biophysics of the cell:
def insert_biophysics(cell, params_dict):
    for sec in cell.allseclist:
        if 'soma' in sec.name():
            # print('soma')
            sec.insert('na')
            sec.gbar_na = params_dict['na_soma']
            sec.insert('Kv')
            sec.gbar_Kv = params_dict['Kv_soma']
            sec.insert('cad')
            sec.insert('ca')
            sec.gbar_ca = params_dict['gca_soma']
            sec.insert('kca')
            sec.gbar_kca = params_dict['gkca_soma']
            sec.insert('it2')
            sec.gcabar_it2 = params_dict['git2_dend']
            sec.insert('Kv1')
            sec.gbar_Kv1 = params_dict['Kv1_soma']
            sec.insert('Kv7')
            sec.gbar_Kv7 = params_dict['Kv7_soma']
            
        elif 'axon' in sec.name():
            # print('axon')
            
            sec.insert('nax')
            sec.gbar_nax = params_dict['na_collat']
            sec.insert('Kv1')
            sec.gbar_Kv1 = params_dict['Kv1_collat']
            
        elif 'ais' in sec.name():
            # print('ais')
            sec.insert('nax')
            sec.gbar_nax = params_dict['na_ais']
            sec.insert('na')
            sec.gbar_na = params_dict['na_soma']
            sec.insert('Kv')
            sec.gbar_Kv = params_dict['Kv_soma']
            sec.insert('Kv1')
            sec.gbar_Kv1 = params_dict['Kv1_ais']
            sec.insert('Kv7')
            sec.gbar_Kv7 = params_dict['Kv7_ais']

            sec.insert('cad')
            sec.insert('ca')
            sec.gbar_ca = params_dict['gca_ais']
            sec.insert('kca')
            sec.gbar_kca = params_dict['gkca_ais']
            sec.insert('it2')
            sec.gcabar_it2 = params_dict['git2_ais']
            
        if 'dend' not in sec.name():
            sec.ena = params_dict['ena']
            sec.ek = params_dict['ek']
        # passive
        sec.insert('pas')
        sec.g_pas = params_dict['g_pas']
        sec.e_pas = params_dict['e_pas']
        sec.Ra = params_dict['ra']
        sec.cm = params_dict['cm']

def compute_ec_peaks(trace, cell):
    amplitudes_per_phase = np.zeros(3)
    t_per_phase = np.zeros(3)
    amplitudes_per_phase[:] = np.nan
    t_per_phase[:] = np.nan
    # first find the second phase
    mini = np.argmin(trace)
    t_per_phase[1]   = np.asarray(cell.tvec[mini])
    amplitudes_per_phase[1] = np.asarray(trace[mini])
    # then search for the first phase
    inds, _ = scipy.signal.find_peaks(trace[1:mini])
    valid_pks = cell.tvec[mini] - cell.tvec[inds] < 1
    if sum(valid_pks) > 1:
        pass
        #print('> Check this case: Mulitple candidate peaks confuse me!')
    elif sum(valid_pks)==1:
        t_per_phase[0]   = cell.tvec[inds][valid_pks]
        amplitudes_per_phase[0] = trace[inds][valid_pks]

    # finally the third phase
    inds, _ = scipy.signal.find_peaks(trace[mini:])
    inds += mini
    valid_pks = cell.tvec[inds] - cell.tvec[mini]< 1
    if sum(valid_pks) > 1:
        pass
        #print('> Check this case: Mulitple candidate peaks confuse me!')
    elif sum(valid_pks)==1:
        t_per_phase[2] = cell.tvec[inds][valid_pks]
        amplitudes_per_phase[2] = trace[inds][valid_pks]

    return amplitudes_per_phase, t_per_phase
