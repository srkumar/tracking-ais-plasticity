#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 12 23:27:41 2020
@author: sreedhar s kumar, tobias gaenswein, alessio buccino
"""
import LFPy
import numpy as np
from pathlib import Path
import scipy.signal
import neuron
import platform


def replace_axon_with_hillock_ais(icell=None, l_hillock=5, l_ais=30, l_axon=1000, d_axon=1, seg_len=5, axon_nseg=5):
    """Replace axon by an hillock and an AIS while keeping the 3d informations of the sections"""
    # Function was modified by Tobi and Sreedhar to solve the issue of disappearing AIS during distal ais relocation
    
    ''' In this first part, we will increase the number of 3d informations in all the axonal sections in order to 
    get the most precise information about axonal morphology. This is done by interpolating several time the existing 
    3d informations (x, y, z, and diameter)'''

    for index, section in enumerate(icell.axonal):
        # first, we create 4 list to store the 3d informations
        list_x3d = []
        list_y3d = []
        list_z3d = []
        list_diam3d = []
        for i in range(int(neuron.h.n3d(sec=section))):  # then, we store the 3d informations in those lists
            list_x3d.append(neuron.h.x3d(i, sec=section))
            list_y3d.append(neuron.h.y3d(i, sec=section))
            list_z3d.append(neuron.h.z3d(i, sec=section))
            list_diam3d.append(neuron.h.diam3d(i, sec=section))

        # this number define the number of interpolations made. The higher it is,
        # the most precise will be the axon 3d definition.
        interpolation_number = 7
        # we create 4 intp lists to store the values corresponding at the middle between each existing values
        for j in range(interpolation_number):
            list_x3d_intp = []
            list_y3d_intp = []
            list_z3d_intp = []
            list_diam3d_intp = []
            for i in range(len(
                    list_x3d) - 1):  # we store the the values corresponding at the middle between each existing values
                list_x3d_intp.append((list_x3d[i] + list_x3d[i + 1]) / 2)
                list_y3d_intp.append((list_y3d[i] + list_y3d[i + 1]) / 2)
                list_z3d_intp.append((list_z3d[i] + list_z3d[i + 1]) / 2)
                list_diam3d_intp.append((list_diam3d[i] + list_diam3d[i + 1]) / 2)
            # we create 4 new lists to store the existing values and the values in between to obtain the lists
            # interpolated
            list_x3d_new = []
            list_y3d_new = []
            list_z3d_new = []
            list_diam3d_new = []
            for i in range(len(list_x3d) - 1):
                list_x3d_new.append(list_x3d[i])
                list_x3d_new.append(list_x3d_intp[i])
                list_y3d_new.append(list_y3d[i])
                list_y3d_new.append(list_y3d_intp[i])
                list_z3d_new.append(list_z3d[i])
                list_z3d_new.append(list_z3d_intp[i])
                list_diam3d_new.append(list_diam3d[i])
                list_diam3d_new.append(list_diam3d_intp[i])
            list_x3d_new.append(list_x3d[-1])
            list_y3d_new.append(list_y3d[-1])
            list_z3d_new.append(list_z3d[-1])
            list_diam3d_new.append(list_diam3d[-1])

            # we erase the firsts lists and put the new values interpolated in it
            list_x3d = list_x3d_new[:]
            list_y3d = list_y3d_new[:]
            list_z3d = list_z3d_new[:]
            list_diam3d = list_diam3d_new[:]

            # we erase the 3d info from the section and replace with the new interpolated 3d info
        neuron.h.pt3dclear(sec=section)
        for i in range(len(list_x3d)):
            neuron.h.pt3dadd(list_x3d[i], list_y3d[i], list_z3d[i], list_diam3d[i], sec=section)

    l_total = l_hillock + l_ais
    final_seg_length = seg_len  # the final length of the segments during the simulation

    # lists to append the future 3d info of hillock
    x3d_hillock = []
    y3d_hillock = []
    z3d_hillock = []
    diam3d_hillock = []
    # lists to append the future 3d info of ais
    x3d_ais = []
    y3d_ais = []
    z3d_ais = []
    diam3d_ais = []

    # calc hillock pos
    dist_from_soma = 0
    for index, section in enumerate(icell.axonal):

        for i in range(int(neuron.h.n3d(sec=section))):

            if i == 0:
                dist_from_soma = dist_from_soma
            else:
                # this line increases the distance from the soma at each new 3d info point
                dist_from_soma = dist_from_soma + neuron.h.arc3d(i, sec=section) - neuron.h.arc3d(i - 1,
                                                                                                          sec=section)

            if dist_from_soma <= l_hillock:
                x3d_hillock.append(neuron.h.x3d(i, sec=section))
                y3d_hillock.append(neuron.h.y3d(i, sec=section))
                z3d_hillock.append(neuron.h.z3d(i, sec=section))
                diam3d_hillock.append(neuron.h.diam3d(i, sec=section))
                
    # delete axon         
    for section in icell.axonal:
        neuron.h.delete_section(sec=section)

    # create, insert and connect hillock to soma
    neuron.h.execute("create hillock[1]", icell)
    icell.hillockal.append(sec=icell.hillock[0])
    icell.all.append(sec=icell.hillock[0])
    icell.hillock[0].connect(icell.soma[0], 1.0, 0.0)
    for i in range(len(x3d_hillock)):
        neuron.h.pt3dadd(x3d_hillock[i], y3d_hillock[i], z3d_hillock[i], diam3d_hillock[i], sec=icell.hillock[0])
    icell.hillock[0].nseg = 1 + int(l_hillock / final_seg_length)

    # create axon, insert and connect to hillock 
    neuron.h.execute("create axon[1]", icell)
    icell.axonal.append(sec=icell.axon[0])
    icell.all.append(sec=icell.axon[0])
    icell.axon[0].nseg = axon_nseg
    icell.axon[0].L = l_axon
    icell.axon[0].diam = d_axon  # diams[count-1]
    icell.axon[0].connect(icell.hillock[0], 1.0, 0.0)

    # get the xyz of the axon
    list_x3d_axon = []
    list_y3d_axon = []
    list_z3d_axon = []
    list_diam3d_axon = []   
    for ii in range(int(l_axon)):
        list_x3d_axon.append(x3d_hillock[-1])
        list_y3d_axon.append(y3d_hillock[-1]-ii)
        list_z3d_axon.append(z3d_hillock[-1])
        list_diam3d_axon.append((diam3d_hillock[-1]-d_axon)*np.exp(-0.5*ii)+d_axon)
    
    for i in range(len(list_x3d_axon)):
        neuron.h.pt3dadd(list_x3d_axon[i], list_y3d_axon[i], list_z3d_axon[i], list_diam3d_axon[i], sec=icell.axon[0])
    
    
    # get pos of ais
    dist_from_soma = l_hillock
    for index, section in enumerate(icell.axonal):

        for i in range(int(neuron.h.n3d(sec=section))):
           if i == 0:
                dist_from_soma = dist_from_soma
           else:
                # this line increases the distance from the soma at each new 3d info point
               dist_from_soma = dist_from_soma + neuron.h.arc3d(i, sec=section) - neuron.h.arc3d(i - 1,
                                                                                                       sec=section)
           if dist_from_soma <= l_total:
               x3d_ais.append(neuron.h.x3d(i, sec=section))
               y3d_ais.append(neuron.h.y3d(i, sec=section))
               z3d_ais.append(neuron.h.z3d(i, sec=section))
               diam3d_ais.append(neuron.h.diam3d(i, sec=section))

           else:
               break
        if dist_from_soma > l_total:
            break

    '''In this third part, we will delete all the axon sections, create hillock and ais section, 
    add the 3d info to these new sections, and connect them'''
    # delete axon
    for section in icell.axonal:
        neuron.h.delete_section(sec=section)
  
    # create ais, insert and connect to hillock
    neuron.h.execute("create ais[1]", icell)
    icell.axon_initial_segment.append(sec=icell.ais[0])
    icell.all.append(sec=icell.ais[0])



    for i in range(len(x3d_ais)):
        neuron.h.pt3dadd(x3d_ais[i], y3d_ais[i], z3d_ais[i], diam3d_ais[i], sec=icell.ais[0])

    icell.ais[0].nseg = 1 + int(l_ais / final_seg_length)

    # childsec.connect(parentsec, parentx, childx)
    icell.ais[0].connect(icell.hillock[0], 1.0, 0.0)

    '''In this fourth part, we will create a myelin section and connect it to the ais section'''

    neuron.h.execute("create axon[1]", icell)
    icell.axonal.append(sec=icell.axon[0])
    icell.all.append(sec=icell.axon[0])
    icell.axon[0].nseg = axon_nseg
    icell.axon[0].L = l_axon
    icell.axon[0].diam = d_axon  # diams[count-1]
    icell.axon[0].connect(icell.ais[0], 1.0, 0.0)

    diams_hillock = []
    diams_ais = []
    for seg in icell.hillock[0]:
        diams_hillock.append(seg.diam)
    for seg in icell.ais[0]:
        diams_ais.append(seg.diam)
# end of function
    
#%% Create empty template
def create_empty_template(template_name, seclist_names=None, secarray_names=None):
    '''create an hoc template named template_name for an empty cell'''
    objref_str = 'objref this, CellRef'
    newseclist_str = ''

    if seclist_names:
        for seclist_name in seclist_names:
            objref_str += ', %s' % seclist_name
            newseclist_str += \
                '             %s = new SectionList()\n' % seclist_name

    create_str = ''
    if secarray_names:
        create_str = 'create '
        create_str += ', '.join(
            '%s[1]' % secarray_name
            for secarray_name in secarray_names)
        create_str += '\n'

    template = '''\
    begintemplate %(template_name)s
      %(objref_str)s
      proc init() {\n%(newseclist_str)s
        forall delete_section()
        CellRef = this
      }

      gid = 0

      proc destroy() {localobj nil
        CellRef = nil
      }

      %(create_str)s
    endtemplate %(template_name)s
           ''' % dict(template_name=template_name, objref_str=objref_str,
                      newseclist_str=newseclist_str,
                      create_str=create_str)
    return template


# Helper function to define the biophysics of the cell:
def insert_biophysics(cell, params_dict):
    for sec in cell.allseclist:
        if 'soma' in sec.name():
            # print('soma')
            sec.insert('na')
            sec.gbar_na = params_dict['na_soma']
            sec.insert('Kv')
            sec.gbar_Kv = params_dict['Kv_soma']
            sec.insert('cad')
            sec.insert('ca')
            sec.gbar_ca = params_dict['gca_soma']
            sec.insert('kca')
            sec.gbar_kca = params_dict['gkca_soma']
            sec.insert('it2')
            sec.gcabar_it2 = params_dict['git2_dend']
            sec.insert('Kv1')
            sec.gbar_Kv1 = params_dict['Kv1_soma']
            sec.insert('Kv7')
            sec.gbar_Kv7 = params_dict['Kv7_soma']
            
        elif 'hillock' in sec.name():
            # print('hillock')
            sec.insert('nax')
            sec.gbar_nax = params_dict['na_collat']
            sec.insert('Kv1')
            sec.gbar_Kv1 = params_dict['Kv1_collat']
           
        elif 'axon' in sec.name():
            # print('axon')
            sec.insert('nax')
            sec.gbar_nax = params_dict['na_collat']
            sec.insert('Kv1')
            sec.gbar_Kv1 = params_dict['Kv1_collat']
            
        elif 'ais' in sec.name():
            # print('ais')
            sec.insert('nax')
            sec.gbar_nax = params_dict['na_ais']
            sec.insert('na')
            sec.gbar_na = params_dict['na_soma']
            sec.insert('Kv')
            sec.gbar_Kv = params_dict['Kv_soma']
            sec.insert('Kv1')
            sec.gbar_Kv1 = params_dict['Kv1_ais']
            sec.insert('Kv7')
            sec.gbar_Kv7 = params_dict['Kv7_ais']

            sec.insert('cad')
            sec.insert('ca')
            sec.gbar_ca = params_dict['gca_ais']
            sec.insert('kca')
            sec.gbar_kca = params_dict['gkca_ais']
            sec.insert('it2')
            sec.gcabar_it2 = params_dict['git2_ais']
            
        # passive
        sec.insert('pas')
        sec.g_pas = params_dict['g_pas']
        sec.e_pas = params_dict['e_pas']
        sec.Ra = params_dict['ra']
        sec.cm = params_dict['cm']
        
def reset_morphology(mycell, morphology_path='../morphology.asc'):
    neuron.h.load_file('stdrun.hoc')
    neuron.h.load_file('import3d.hoc')
    
    
    extension = morphology_path.split('.')[-1]
    
    if extension.lower() == 'swc':
        imorphology = neuron.h.Import3d_SWC_read()
    elif extension.lower() == 'asc':
        imorphology = neuron.h.Import3d_Neurolucida3()
    else:
        raise ValueError("Unknown filetype: %s" % extension)

    imorphology.quiet = 1
    
    if platform.system() == 'Windows':
        neuron.h.hoc_stdout('NUL')
    else:
        neuron.h.hoc_stdout('/dev/null')
    
    imorphology.input(str(morphology_path))
    neuron.h.hoc_stdout()
    
    morphology_importer = neuron.h.Import3d_GUI(imorphology, 0)
    morphology_importer.instantiate(mycell)        

def compute_ec_peaks(trace, cell):
    amplitudes_per_phase = np.zeros(3)
    t_per_phase = np.zeros(3)
    amplitudes_per_phase[:] = np.nan
    t_per_phase[:] = np.nan
    # first find the second phase
    mini = np.argmin(trace)
    t_per_phase[1]   = np.asarray(cell.tvec[mini])
    amplitudes_per_phase[1] = np.asarray(trace[mini])
    # then search for the first phase
    inds, _ = scipy.signal.find_peaks(trace[1:mini])
    valid_pks = cell.tvec[mini] - cell.tvec[inds] < 1
    if sum(valid_pks) > 1:
        pass
        #print('> Check this case: Mulitple candidate peaks confuse me!')
    elif sum(valid_pks)==1:
        t_per_phase[0]   = cell.tvec[inds][valid_pks]
        amplitudes_per_phase[0] = trace[inds][valid_pks]

    # finally the third phase
    inds, _ = scipy.signal.find_peaks(trace[mini:])
    inds += mini
    valid_pks = cell.tvec[inds] - cell.tvec[mini]< 1
    if sum(valid_pks) > 1:
        pass
        #print('> Check this case: Mulitple candidate peaks confuse me!')
    elif sum(valid_pks)==1:
        t_per_phase[2] = cell.tvec[inds][valid_pks]
        amplitudes_per_phase[2] = trace[inds][valid_pks]

    return amplitudes_per_phase, t_per_phase
