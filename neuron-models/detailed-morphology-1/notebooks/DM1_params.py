#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Parameter definitions for detailed morphology -1 neuron model
"""
import neuron
import numpy as np
from pathlib import Path
import time
import MEAutility as mu

# Decide on data saving params
save_data = False # use false for a demo. If true, make sure you set path_root to a valid directory
path_root = ""

# Create cell model based on [Hallerman et al. 2012](https://www.nature.com/articles/nn.3132)

#%% morphological params (in um)
# paisl == proximal ais location
d_dend  = 2
d_soma  = 16
d_axon  = 1
d_ais   = 1
l_dend  = 1000
l_ais   = 30
l_axond = 1000

#%% Define the min and max proximal ais locations (paisl) in um
# to set all_paisl to a scalar, set max_paisl to min_paisl + 1
min_paisl  = 40
max_paisl  = min_paisl + 1
paisl_step = 1
all_paisl  = np.arange(min_paisl, max_paisl, paisl_step)
num_paisl  = len(all_paisl)


#%% Define the various angles of rotation in degrees
# to set theta_vec to a scalar, set theta_max to theta_min + 1
theta_min  = 0 # set min angle # -180
theta_max  = theta_min + 1 # set max angle # 181
theta_step = 1 # set steps of angle
theta_vec  = np.arange(theta_min, theta_max, theta_step)

#%% Define the various positions of the soma to generate the positional jitter
soma_pos_vec = np.array([[0., 0., 0.], [8.75, 0. ,0.], [0., 0., 8.75], [8.75, 0., 8.75]])

# Extracellular cutouts
num_datapoints = 961 # samples per cutout

#%% Set location to save data
if save_data == True:
    path_suffix = time.strftime("%y%m%d-%H%M")
    data_dir = Path(path_root + path_suffix)
    data_dir.mkdir(parents=True, exist_ok=True)

    
#%% Set path to morphology
morphology_path = "../morphology.asc"

#%% Set section names
seclist_names = ['all', 'somatic', 'basal', 'apical', 'axon_initial_segment', 'hillockal', 'myelinated', 
                         'axonal']
secarray_names = ['soma', 'dend', 'apic', 'ais', 'hillock', 'myelin', 'axon']

#%% Other neuron parameters [Change with caution]
ca_reduce_fac = 0.1
v_init        = -85
rm            = 15000

params_dict = {
    'rm' : rm,
    'ra' : 80,
    'cm' : 1,
    'celsius' : 33,
    'v_init' : v_init,
    'e_pas' : v_init,
    'g_pas' : 1. / rm,

    'na_ais': 7000,
    'na_node': 7000,
    'na_collat': 500,
    'na_soma': 500,
    'na_dend': 20 ,
    'na_myelin': 40,
    'max_distance_apic': 300,  #distance where gbar_na has reached na_dend
    'max_distance_basal': 100,  #corresponding distance for basal dendrites


    'length_constant_Kv_and_Kv1': 80,  # length constant of the exponential decay in um of Kv and Kv1 according to Keren et al., 2009, JPhysiol, 587:1413-37
    'Kv_soma' : 100,
    'Kv_dend' : 3,

    'Kv1_dend': 0.3,
    'Kv1_ais': 2000, 
    'Kv1_soma': 100,
    'Kv1_collat': 400,

    'Kv7_soma': 1,
    'Kv7_dend': 1,
    'Kv7_ais': 7,

    'gca_dend': 2.0*ca_reduce_fac,
    'gca_soma': 2.0*ca_reduce_fac,
    'gca_ais': 0.0,


    'git2_ais': 0.0,
    'git2_dend': 2.0*ca_reduce_fac,
    'git2_apic': 6.0*ca_reduce_fac,
    'git2_soma': 2.0*ca_reduce_fac,

    'gkca_soma': 1.0*ca_reduce_fac,
    'gkca_dend': 1.0*ca_reduce_fac,
    'gkca_ais': 1.0*ca_reduce_fac,

    'gCa_HVA_apic_hot_fac': 1, #i.e. no Ca hot spot//ca
    'gCa_LVAst_apic_hot_fac': 1, #it2
    'gCa_hot_start': 685,
    'gCa_hot_end': 885,

    'spinescale': 1.5,
    'sheaths': 10, #number of myelin sheaths

    'ena': 55,
    'ek': -98,
}

# some params are defined in NEURON interpeter directly
neuron.h('vShift_na=10') # affecting activation and inactivation
neuron.h('vShift_inact_na=10') # affects only inactivation
neuron.h('q10_na=3')
neuron.h('q10h_na=3')

neuron.h('vShift_nax=10')
neuron.h('vShift_inact_nax=10')
neuron.h('q10_nax=3.0')
neuron.h('q10h_nax=3.0')

neuron.h('vShift_Kv1=10')
neuron.h('vShift_inact_Kv1=-15')

# Define other parameters for instantiating the cell model>

cell_kwargs = {'nsegs_method': 'lambda100', 'max_nsegs_length': 2, 'tstop': 30, 'dt': 2**-5, 
               'v_init': params_dict['v_init'], 'celsius': params_dict['celsius']}


#%% Define HD-MEA
mea_info = {'dim': [30, 30],
            'electrode_name': 'hd-mea',
            'pitch': [17.5, 17.5],
            'shape': 'rect',
            'size': [9.3/2, 5.45/2],
            'type': 'mea',
            'plane': 'xy'}
hdmea = mu.return_mea(info=mea_info)
hdmea.move([0, 0, 11.6])
elxypos = hdmea.positions[:, [0, 1]]
num_electrodes_per_direction = 6
dist_per_direction = mea_info['pitch'][0]/2 + (num_electrodes_per_direction-1)*mea_info['pitch'][0]
xposlim = dist_per_direction*np.array([-1, 1])
yposlim = dist_per_direction*np.array([-1, 1])
elmask_x = np.logical_and(
    elxypos[:, 0] >= xposlim[0], elxypos[:, 0] <= xposlim[1])
elmask_y = np.logical_and(
    elxypos[:, 1] >= yposlim[0], elxypos[:, 1] <= yposlim[1])
elmask = np.logical_and(elmask_x, elmask_y)
elnumpos = elxypos/hdmea.pitch[0]
selxypos = elnumpos[elmask]
selxy = elxypos[elmask]


#%% Define stimulation params
stim = 'iclamp' # or syn
stim_point = [0, 0, 0]
syn_input_times = [5]
syn_params = {'idx' : [],                             # idx is set in main script                
              'e' : 0,                                # reversal potential
              'syntype' : 'ExpSyn',                   # synapse type
              'tau' : 2,                              # syn. time constant ms
              'weight' : 0.02,                        # syn. weight
              'record_current' : True                 # syn. current record
    }
clamp_params = {'idx' : [],
                'pptype' : 'IClamp',                   # IClamp point process
                'dur' : 20,                            # dur in ms
                'amp' : 1,                           # amp in nA
                'delay' : 5                            # delay in ms
    }
