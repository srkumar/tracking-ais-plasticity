# tracking-ais-plasticity

Neuron models with example notebooks and scripts to generate the figures in the manuscript: "Tracking axon initial segment plasticity using high-density microelectrode arrays: A computational study"

## Getting started

- Create a conda environment using the environment.yml file included in the repo.
  conda env create -f environment.yml
 
## Bugs/Issues
In case you encounter issues with setting up the environment, fetching the packages, or running the code, please feel free to write us (contact details below)


## Authors
Tobias Gaenswein (tobias.gaenswein@bsse.eth.ch)
Sreedhar Kumar (sreedhar.kumar@bsse.eth.ch)
Alessio Buccino (alessio.buccino@bsse.ethz.ch)

## License
Copyright (c) 2022, Tobias Gaenswein, Sreedhar Kumar, Alessio Buccino
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## Acknowledgements
We thank the following MathWorks File Exchange contributors:
- Kristjan Jonasson (2022). RGB triple of color name, version 2 (https://www.mathworks.com/matlabcentral/fileexchange/24497-rgb-triple-of-color-name-version-2), MATLAB Central File Exchange. Retrieved August 11, 2022.
- Crameri, F. (2019). Scientific Colour Maps. Zenodo. Retrieved from https://zenodo.org/record/1243862 (via Chad Greene)
- Rob Campbell (2022). raacampbell/shadedErrorBar (https://github.com/raacampbell/shadedErrorBar), GitHub. Retrieved August 11, 2022. 
