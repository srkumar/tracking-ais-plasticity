function varargout = drawFootprint(waveforms_cols, xy, args)
%% Input argument options
arguments
    waveforms_cols {mustBeNumeric}
    xy (:, 2) {mustBeNumeric}
    args.channels {mustBeNumeric} = [] % if you need to subselect or number
    args.axh  {mustBeScalarOrEmpty} = gobjects(0)
    args.chlabel (1,1) logical=false
    args.tracecolor (1,3) {mustBeVector, mustBeInRange(args.tracecolor, 0, 1)} = rgb('Black')
    args.linew (1,1) double = 1.5
    args.showbaseline (1,1) logical = false
    args.ScaleFactor (1,1) struct = struct('x', 0, 'y', 0)
    args.vgain (1,1) double = 1
    args.fs (1,1) double = 20e3
    args.titlestr (1,1) = ""
    args.titlesize (1,1) double = 8
    args.addscalebar (1,1) logical = false
    args.ScaleBar (1,1) struct = struct('V', 1e2, 't', 3e-3)
    args.ScaleBarOffset (1,1) struct = struct('x', 0, 'y', 0)
    args.specialfilter (1,1) logical = false
    args.selectedidx = []
    args.showselected (1,1) logical = false
    args.hideplot (1,1) logical = true
    args.density (1,1) string {mustBeMember(args.density, ["compact", "comfortable", "realistic"])} = "compact"
    args.electrodesonly (1,1) logical = false
    args.c_face_electrodes {mustBeVector} = [255, 165, 0]/255;
    args.c_edge_electrodes {mustBeVector} = [154, 133, 98]/255;
    args.a_face_electrodes (1,1) {mustBeScalarOrEmpty} = 0.75;
    args.a_edge_electrodes (1,1) {mustBeScalarOrEmpty} = 0.75;
end

%% Plot the traces in spatial context
waveforms_cols = waveforms_cols * args.vgain;
waveforms_rows = waveforms_cols.';
visible = 'on';
if ~args.ScaleFactor.x || ~args.ScaleFactor.y
    args.ScaleFactor = autoScale(waveforms_rows, xy, args.density);
end

if isempty(args.axh)
    if args.hideplot, visible = 'off'; end
    figure('Color', 'w', 'Visible', visible);
    args.axh = gca;
end

% axes(axh);

if isempty(args.channels)
    args.channels = 1:size(waveforms_cols, 2);
end

trace_vector      = zeros(2,size(waveforms_rows, 2));
trace_vector(1,:) = (1:size(waveforms_rows, 2)) - size(waveforms_rows, 2)/2;

if args.showbaseline
    baseline    = [1:size(waveforms_rows, 2); zeros(1,size(waveforms_rows, 2))]; % baseline vector
    basecolor   = args.tracecolor*0.8; % color for baseline
end

hold on;

for ii=1:length(args.channels)
    trace_vector(2,:) = waveforms_rows(ii,:);
    tmp_x_pos = xy(ii,1)*args.ScaleFactor.x;
    tmp_y_pos = xy(ii,2)*args.ScaleFactor.y;

    if args.showbaseline
        plot(baseline(1,:)+tmp_x_pos, baseline(2,:)+tmp_y_pos, 'LineStyle', '-',...
            'Color', basecolor, 'linew', args.linew);
    end
    if args.specialfilter
        plot(trace_vector(1,:)+tmp_x_pos, smooth(trace_vector(2,:)+tmp_y_pos, 7, 'sgolay'),...
            'LineStyle', '-', 'Color', args.tracecolor, 'linew', args.linew);
    elseif args.electrodesonly
        el_x = [xy(ii,1)-9.3/2, xy(ii,1)-9.3/2, xy(ii,1)+9.3/2, xy(ii,1)+9.3/2]*args.ScaleFactor.x;
        el_y = [xy(ii,2)-5.45/2, xy(ii,2)+5.45/2, xy(ii,2)+5.45/2, xy(ii,2)-5.45/2]*args.ScaleFactor.y;
        el_shape  = polyshape(el_x, el_y);

        plot(el_shape, "FaceColor", args.c_face_electrodes, "EdgeColor", args.c_edge_electrodes, "FaceAlpha", args.a_face_electrodes, "EdgeAlpha", args.a_edge_electrodes, "LineWidth", 2.5);
    else
        plot(trace_vector(1,:)+tmp_x_pos, trace_vector(2,:)+tmp_y_pos,...
            'LineStyle', '-', 'Color', args.tracecolor, 'linew', args.linew);
    end
    if args.chlabel
        text(tmp_x_pos, tmp_y_pos-args.ScaleFactor.y*5, num2str(args.channels(ii)), 'FontSize', 8, 'HorizontalAlignment', 'right','Rotation',30);
    elseif args.showselected && ismember(args.channels(ii), args.selectedidx)
        idx_disp = find(ismember(args.selectedidx, args.channels(ii)));
        text(tmp_x_pos, tmp_y_pos-args.ScaleFactor.y*5, num2str(idx_disp), 'FontSize', 6, 'HorizontalAlignment', 'right','Rotation',30);
    end
end

if args.addscalebar
    tmp_sc_x = max(xy(:,1))*args.ScaleFactor.x;
    tmp_sc_y = (min(xy(:,2)) + 0.5*range(xy(:,2)) - abs(mode(diff(xy(:,2)))))*args.ScaleFactor.y;

    % additional offsets
    tmp_sc_x = tmp_sc_x + args.ScaleFactor.x * args.ScaleBarOffset.x;
    tmp_sc_y = tmp_sc_y + args.ScaleFactor.y * args.ScaleBarOffset.y;

    %position the scalebar
    line(tmp_sc_x*[1;1], tmp_sc_y-([0; args.ScaleBar.V]), 'LineWidth', 1.5, 'Color', 'k');
    line(tmp_sc_x+[args.ScaleBar.t*args.fs; 0], tmp_sc_y*[1;1], 'LineWidth', 1.5, 'Color', 'k');

    %position its text
    text(tmp_sc_x - args.ScaleFactor.x*5, tmp_sc_y-args.ScaleBar.V/2, [num2str(args.ScaleBar.V), ' \muV'], 'FontSize', 8, 'HorizontalAlignment', 'center', 'Rotation', 90);
    text(tmp_sc_x+args.fs/1e3, tmp_sc_y+args.ScaleFactor.y*1, [num2str(args.ScaleBar.t*1e3), ' ms'], 'FontSize', 8, 'VerticalAlignment', 'top');
end
set(gca,'XTickLabel','','YTickLabel','');
axis tight;
% axis ij;
axis off;
title(args.titlestr, 'FontSize', args.titlesize, 'Color', rgb('Gray'), 'FontWeight', 'normal', 'Interpreter', 'None');

switch(nargout)
    case 0
        varargout = {};
    case 1
        varargout{1} = args.axh;
    case 2
        varargout{1} = args.axh;
        varargout{2} = args.ScaleFactor;
    otherwise
        error('Designed for only 2 outputs so far');
end

function scale_factor = autoScale(waveforms_rows, xy, density)
sorted_xy = sort(xy);
delta_x = diff(sorted_xy(:,1));
delta_y = diff(sorted_xy(:,2));
delta_x(~delta_x) = [];
delta_y(~delta_y) = [];
PITCH.X = mode(delta_x);
PITCH.Y = mode(delta_y);
if isnan(PITCH.X) || isnan(PITCH.Y)
    PITCH.X = 35;
    PITCH.Y = 35;
end

TLEN = size(waveforms_rows, 2);

scale_factor.x = ceil((TLEN./PITCH.X)*2)/2 + 1;
scale_factor.y = double(ceil((((max(waveforms_rows, [], 'all') - min(waveforms_rows, [], 'all'))./PITCH.Y))./2)*2);
if ~scale_factor.x || ~scale_factor.y %is still zero
    scale_factor.x = 3;
    scale_factor.y = 6;
end

if density == "comfortable"
    scale_factor.x = scale_factor.x*1.5;
elseif density == "realistic"
    scale_factor.x = scale_factor.y;
    axis equal;
end