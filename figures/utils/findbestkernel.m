function [best_fit_id, best_error, best_prediction] = findbestkernel(x, n_bs, impulse_resp, actual_cutout, offset)
t1 = 1:length(x);
t2 = linspace(t1(1), t1(end), n_bs);
x2 = interp1(t1, x, t2, 'spline');
num_channels = size(impulse_resp, 2);
M = size(impulse_resp, 1);
candidate_pred = zeros(length(x), num_channels);

for kchan=1:num_channels
    h = impulse_resp(:, kchan);
    X = toeplitz(x2, [x2(1) zeros(1, M-1)]);
    y2 = X * h;
    y = interp1(t2, y2, t1, 'spline');
    candidate_pred(:, kchan) = y + offset;
end
pred_errors  = sqrt(mean((actual_cutout - candidate_pred).^2));
[best_error, best_fit_id] = min(pred_errors);
best_prediction = candidate_pred(:, best_fit_id);
end