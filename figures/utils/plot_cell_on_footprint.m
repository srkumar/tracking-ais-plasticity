function plot_cell_on_footprint(params, ScaleFactor, args)
arguments
    params
    ScaleFactor
    args.axh {mustBeScalarOrEmpty} = gobjects(0)
    args.c_ais {mustBeVector} = [0.75, 0, 0]
    args.a_ais {mustBeScalarOrEmpty} = 1;
    args.line_ais {mustBeScalarOrEmpty} = 0.5;
    args.c_f_soma {mustBeVector} = [0.4039, 0.5412, 0.7176] %color soma face
    args.c_e_soma {mustBeVector} = [0.4039, 0.5412, 0.7176] % color soma edge line
    args.a_soma {mustBeScalarOrEmpty} = 0.2;
    args.line_soma {mustBeScalarOrEmpty} = 0.5;
    args.linestyle_soma {mustBeTextScalar} = "-";
    args.c_dend {mustBeVector} = [0.5804, 0.8000, 0.6392]
    args.a_dend {mustBeScalarOrEmpty} = 0.9;
    args.line_dend {mustBeScalarOrEmpty} = 0.5;
    args.c_axon {mustBeVector} = [0.8242, 0.8242, 0.8242]
    args.a_axon {mustBeScalarOrEmpty} = 0.9;
    args.line_axon {mustBeScalarOrEmpty} = 0.5;
    args.ais_len {mustBeScalarOrEmpty} = 30
    args.dia_soma {mustBeScalarOrEmpty} = 16;
end

axh = args.axh;
cell_pos = params.cell_pos;
theta = params.theta;

% calculate soma properties and plot
scaled_soma    = args.dia_soma * ScaleFactor.x;
% Get the limits
mylim.x = xlim; mylim.y = ylim;
original_lim.x = xlim; original_lim.y = ylim;

% calculate the limits of the neurites in the plot
dend_x        = [-1+cell_pos(1), -1+cell_pos(1), 1+cell_pos(1), 1+cell_pos(1)]*ScaleFactor.x;
dend_y        = [(cell_pos(3)+args.dia_soma/2)*ScaleFactor.y, 2*mylim.y(2), 2*mylim.y(2), (cell_pos(3)+args.dia_soma/2)*ScaleFactor.y];
dendrite_shape  = polyshape(dend_x, dend_y);
dendrite_rotate = rotate(dendrite_shape, theta, [cell_pos(1) cell_pos(3)]*ScaleFactor.x);

axon_x          = [-0.5+cell_pos(1), -0.5+cell_pos(1), 0.5+cell_pos(1), 0.5+cell_pos(1)]*ScaleFactor.x;
axon_y          = [2*mylim.y(1), (cell_pos(3)-args.dia_soma/2)*ScaleFactor.y, (cell_pos(3)-args.dia_soma/2)*ScaleFactor.y, 2*mylim.y(1)];
axon_shape      = polyshape(axon_x, axon_y);
axon_rotate     = rotate(axon_shape, theta, [cell_pos(1), cell_pos(3)]*ScaleFactor.x);

ais_x           = [-0.5+cell_pos(1), -0.5+cell_pos(1), 0.5+cell_pos(1), 0.5+cell_pos(1)]*ScaleFactor.x;
ais_y           = [cell_pos(3) - args.dia_soma/2 - params.prox_ais_distance - args.ais_len,...
    cell_pos(3) - args.dia_soma/2 - params.prox_ais_distance,...
    cell_pos(3) - args.dia_soma/2 - params.prox_ais_distance,...
    cell_pos(3) - args.dia_soma/2 - params.prox_ais_distance - args.ais_len] * ScaleFactor.y;
ais_shape       = polyshape(ais_x, ais_y);
ais_rotate      = rotate(ais_shape, theta, [cell_pos(1), cell_pos(3)]*ScaleFactor.x);

hd   = plot(dendrite_rotate, "FaceColor", args.c_dend, "FaceAlpha", args.a_dend,"EdgeColor", args.c_dend, "EdgeAlpha",  args.a_dend, "LineWidth", args.line_dend);
ha   = plot(axon_rotate, 'FaceColor', args.c_axon, "FaceAlpha", args.a_axon, "EdgeColor", args.c_axon, "EdgeAlpha",  args.a_axon, "LineWidth", args.line_axon);

cell_schema = rectangle("Position",...
                [cell_pos(1)*ScaleFactor.x - scaled_soma/2,...
                cell_pos(3)*ScaleFactor.y - scaled_soma/2,...
                scaled_soma, scaled_soma],...
                "Curvature", [1,1],...
                "FaceColor", [args.c_f_soma, args.a_soma],...
                "EdgeColor", args.c_e_soma, ...
                "LineWidth", args.line_soma,...
                "LineStyle", args.linestyle_soma);

hais = plot(ais_rotate, 'FaceColor', args.c_ais, "FaceAlpha", args.a_ais, "EdgeColor", args.c_ais, "EdgeAlpha",  args.a_ais, "LineWidth", args.line_ais);


xlim(original_lim.x)
ylim(original_lim.y)
% set(axh, 'Children', flipud(get(gca, 'Children')));
% legend(hais, "AIS from "+params.prox_ais_distance+" \mum; theta: "+(-theta), 'box', 'off', 'AutoUpdate', 'off', 'Location','northoutside');