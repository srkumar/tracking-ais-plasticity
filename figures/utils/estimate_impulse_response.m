function [imp_resp, yest, RMSE] = estimate_impulse_response(x, y, ir_length)
yest = zeros(length(x), length(ir_length));
RMSE = zeros(1, length(ir_length));
imp_resp = cell(1, length(ir_length));
for mi = 1:length(ir_length)
    M = ir_length(mi);
    X = toeplitz(x, [x(1) zeros(1, M-1)]);      % X : convolution matrix
    h = X \ y;
    r = y - X * h;       % r : residual
    RMSE(mi) = sqrt( sum(r.^2) );
    imp_resp{mi} = h;
    yest(:,mi) = X * h;
end