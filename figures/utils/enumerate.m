classdef enumerate < handle
   properties(Access = private)
      IterationList
   end
   
   methods 
       function self = enumerate(in)
           self.IterationList = in(:).';
       end
       function varargout = subsref(self, S)
           item = subsref(self.IterationList,S);
           idx = S.subs{2};
           out.value = item;
           out.idx = idx;
           varargout = {out};
       end
       function [m,n] = size(self)
           [m,n] = size(self.IterationList);
       end
   end
end