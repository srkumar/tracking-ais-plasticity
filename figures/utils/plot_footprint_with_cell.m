function [fh, varargout] = plot_footprint_with_cell(waveforms, xy, args)
% Plots footprint and overlays a ball and stick model, if selected
arguments
    waveforms
    xy (:, 2)
    args.ScaleFactor (1,1) struct = struct('x', 0, 'y', 0) % changes spacing of individual waveforms in footprint
    args.showcell (1,1) logical = true % overlay cell/not
    args.showearliest (1,1) logical = true % mark lowest latency channel (green)
    args.showmin (1,1) logical = true % mark highest amplitude channel (red)
    args.params (1,1) struct = struct([])
    args.axh {mustBeScalarOrEmpty} = gobjects(0)
    args.hideplot (1,1) logical = false
    args.linew (1,1) double = 1
    args.density (1,1) string {mustBeMember(args.density, ["compact", "comfortable", "realistic"])} = "compact"
    args.fs (1,1) double = 20e3
    args.tracecolor (1,3) {mustBeVector, mustBeInRange(args.tracecolor, 0, 1)} = rgb('Black')
    args.specialchannels {mustBeVector(args.specialchannels, 'allow-all-empties')} = [];
    args.chlabel (1,1) logical=false
    args.channels {mustBeNumeric} = [];
    args.electrodesonly (1,1) logical=false
end

params = args.params;
ScaleFactor = args.ScaleFactor;
if args.showcell
    assert(~isempty(args.params), "To plot cell, please pass in cell_params.");
end

[axh, tmp_sf] = drawFootprint(waveforms, xy, ...
    'ScaleFactor', ScaleFactor, ...
    'tracecolor', args.tracecolor, ...
    'linew', args.linew, ...
    'axh', args.axh, ...
    'hideplot', args.hideplot, ...
    'density', args.density,...
    'fs', args.fs,...
    'chlabel', args.chlabel, ...
    'channels', args.channels,...
    "electrodesonly", args.electrodesonly);

if ~ScaleFactor.x || ~ScaleFactor.y
    ScaleFactor = tmp_sf;
end
axis equal

axis tight;

line_handles = flipud(findobj(axh, "type", "line"));
set(line_handles(args.specialchannels), "Color", rgb("crimson"));
%% If show cell
if args.showcell
 plot_cell_on_footprint(params, ScaleFactor, axh=axh)
end

%% If mark largest amplitude channel
if args.showmin
    [tmp_.all_amps, tmp_.all_ids] = min(waveforms);
    [tmp_.min_amp, ~] = min(tmp_.all_amps);
    tmp_.min_chan_ids = find(tmp_.all_amps == tmp_.min_amp);
    scatter(xy(tmp_.min_chan_ids, 1).*ScaleFactor.x - size(waveforms, 1)/2, xy(tmp_.min_chan_ids, 2).*ScaleFactor.y, 75, rgb('Crimson'), 'filled', '^', 'MarkerFaceAlpha', 0.5);
    if numel(tmp_.min_chan_ids)>1 % plot center of mass in case there are multiple candidates
        com.xy = mean(xy(tmp_.min_chan_ids,:)).* [ScaleFactor.x, ScaleFactor.y];
        plot(com.xy(1), com.xy(2), 'o', 'Color', rgb('Crimson'), 'LineWidth', 2, 'MarkerSize', 6);
    end
    clearvars tmp_* com
end

%% if mark lowest latency channel
if args.showearliest
    [tmp_.all_amps, tmp_.all_ids] = min(waveforms);
    % amplitude criterion to search for low latency channels (e.g., 20%)
    threshold_perc = 25/100;
    [tmp_.min_amp, ~] = min(tmp_.all_amps);
    tmp_.selected_els = find(tmp_.all_amps <= (threshold_perc * tmp_.min_amp));
    [tmp_.min_latency, ~] = min(tmp_.all_ids(tmp_.selected_els));
    tmp_.earliest_chan_ids = tmp_.selected_els(tmp_.all_ids(tmp_.selected_els) == tmp_.min_latency);
    plot(xy(tmp_.earliest_chan_ids, 1).*ScaleFactor.x - size(waveforms, 1)/2, xy(tmp_.earliest_chan_ids, 2).*ScaleFactor.y, '.', 'MarkerSize', 10, 'Color', rgb('ForestGreen'));
    if numel(tmp_.earliest_chan_ids)>1 % plot center of mass in case there are multiple candidates
        com.xy = mean(xy(tmp_.earliest_chan_ids,:)).* [ScaleFactor.x, ScaleFactor.y];
        plot(com.xy(1), com.xy(2), 'o', 'Color', rgb('ForestGreen'), 'LineWidth', 2, 'MarkerSize', 6);
    end
    clearvars tmp_* com
end
fh = gcf;

if nargout == 2
    varargout{1} = ScaleFactor;
end