%% Script to plot figure 2
% This script creates the schematic of the ball-and-stick neuron model on
% the simulated HD-MEA as well as the footprint plots with the
% ball-and-stick neuron model overlay.

%% Initialize the script
location = matlab.desktop.editor.getActiveFilename;
[FilePath, ~, ~] = fileparts(location);
cd(FilePath)
addpath(genpath("../utils"))
load("Figure2.mat")

%% Subplot A
% We simulated the neuron at four different somatic positions to account
% for extracellular footprint variability due to slight differences in the 
% positions of the neural compartments relative to the electrodes. Neuron 
% positions are indicated by the blue dashed circles, and the black dots 
% represent the center of the soma. Additionally, the entire neuron was 
% rotated 360° at 1° increments for each cell position to simulate the 
% extracellular footprints pertaining to various possible neuronal 
% orientations

% Plot the electrode array
ScaleFactor = struct("x", 7, "y", 7);
fh = figure('units','normalized','outerposition',[0 0 1 1]); axh = gca;
[axh, ~] = drawFootprint(FigureData.Waveforms.AIS_5um.theta_0, ...
    FigureData.ElectrodeLocation, ...
    'ScaleFactor', ScaleFactor, ...
    'axh', axh, ...
    'density', 'compact',...
    'fs', FigureData.fs,...
    "electrodesonly", true,...
    "a_edge_electrodes", 0);

axis equal
axis tight


% Plot the cell in different orientations on top
theta_vec = [0,30,60];
params.cell_pos = [0, 0, 0];
params.prox_ais_distance = 5;
for ii = 1:numel(theta_vec)
    params.theta = theta_vec(ii);
    if params.theta == 0
        plot_cell_on_footprint(params, ScaleFactor, axh=axh, a_ais=1, a_axon=0.5,...
            a_dend=0.84, a_soma=0.6, c_ais=[192,0,0]/255, c_dend=[12,130,0]/255, c_axon=[124,124,124]/255)
    else
        plot_cell_on_footprint(params, ScaleFactor, axh=axh, a_ais=0.3, a_axon=0.3,...
            a_dend=0.3, a_soma=0, c_ais=[192,0,0]/255, c_dend=[12,130,0]/255, c_axon=[124,124,124]/255)
    end
end


% Plot the cell position indications
pos_vec = [{[8.75, 0, 0]}, {[0, 0, 8.75]}, {[8.75, 0, 8.75]}];
params.theta = 0;
for jj = 1:numel(pos_vec)
    params.cell_pos = pos_vec{jj};
    plot_cell_on_footprint(params, ScaleFactor, axh=axh, a_ais=0, a_axon=0,...
        a_dend=0, a_soma=0.0, linestyle_soma="--", line_soma=3)
    scatter(pos_vec{jj}(1)*ScaleFactor.x, pos_vec{jj}(3)*ScaleFactor.y, "ko", "filled")
end
scatter(0,0,"ko", "filled")

%% Subplot B (left)
% Simulated extracellular footprint of a cell with the baseline AIS
% starting 5 μm from the soma

fh = figure('units','normalized','outerposition',[0 0 1 1]); axh = gca;
[axh, ~] = drawFootprint(FigureData.Waveforms.AIS_5um.theta_0, ...
    FigureData.ElectrodeLocation, ...
    'ScaleFactor', ScaleFactor, ...
    'axh', axh, ...
    'density', 'compact',...
    'fs', FigureData.fs,...
    "a_edge_electrodes", 0);

axis equal
axis tight

params.cell_pos = [0, 0, 0];
params.prox_ais_distance = 5;
params.theta = 0;

plot_cell_on_footprint(params, ScaleFactor, axh=axh, a_ais=1, a_axon=0.5,...
    a_dend=0.84, a_soma=0.6, c_ais=[192,0,0]/255, c_dend=[12,130,0]/255, c_axon=[124,124,124]/255)

%% Subplot B (right)
% Simulated extracellular footprint of a cell with the farthest simulated 
% AIS position being at 40 μm distance from the soma

fh = figure('units','normalized','outerposition',[0 0 1 1]); axh = gca;
[axh, ~] = drawFootprint(FigureData.Waveforms.AIS_40um.theta_0, ...
    FigureData.ElectrodeLocation, ...
    'ScaleFactor', ScaleFactor, ...
    'axh', axh, ...
    'density', 'compact',...
    'fs', FigureData.fs,...
    "a_edge_electrodes", 0);

axis equal
axis tight

params.cell_pos = [0, 0, 0];
params.prox_ais_distance = 40;
params.theta = 0;

plot_cell_on_footprint(params, ScaleFactor, axh=axh, a_ais=1, a_axon=0.5,...
    a_dend=0.84, a_soma=0.6, c_ais=[192,0,0]/255, c_dend=[12,130,0]/255, c_axon=[124,124,124]/255)


%% Subplot C (left)
% Simulated extracellular footprint of a cell with the baseline AIS
% starting 5 μm from the soma and rotated clockwise by 30° 

fh = figure('units','normalized','outerposition',[0 0 1 1]); axh = gca;
[axh, ~] = drawFootprint(FigureData.Waveforms.AIS_5um.theta_30, ...
    FigureData.ElectrodeLocation, ...
    'ScaleFactor', ScaleFactor, ...
    'axh', axh, ...
    'density', 'compact',...
    'fs', FigureData.fs,...
    "a_edge_electrodes", 0);

axis equal
axis tight

params.cell_pos = [0, 0, 0];
params.prox_ais_distance = 5;
params.theta = -30;

plot_cell_on_footprint(params, ScaleFactor, axh=axh, a_ais=1, a_axon=0.5,...
    a_dend=0.84, a_soma=0.6, c_ais=[192,0,0]/255, c_dend=[12,130,0]/255, c_axon=[124,124,124]/255)

%% Subplot C (right)
% Simulated extracellular footprint of a cell with the farthest simulated 
% AIS position being at 40 μm distance from the soma and rotated clockwise
% by 30° 

fh = figure('units','normalized','outerposition',[0 0 1 1]); axh = gca;
[axh, ~] = drawFootprint(FigureData.Waveforms.AIS_40um.theta_30, ...
    FigureData.ElectrodeLocation, ...
    'ScaleFactor', ScaleFactor, ...
    'axh', axh, ...
    'density', 'compact',...
    'fs', FigureData.fs,...
    "a_edge_electrodes", 0);

axis equal
axis tight

params.cell_pos = [0, 0, 0];
params.prox_ais_distance = 40;
params.theta = -30;

plot_cell_on_footprint(params, ScaleFactor, axh=axh, a_ais=1, a_axon=0.5,...
    a_dend=0.84, a_soma=0.6, c_ais=[192,0,0]/255, c_dend=[12,130,0]/255, c_axon=[124,124,124]/255)

